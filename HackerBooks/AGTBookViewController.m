//
//  AGTBookControllerViewController.m
//  HackerBooks
//
//  Created by Fernando Rodríguez Romero on 17/04/15.
//  Copyright (c) 2015 Agbo. All rights reserved.
//

#import "AGTBookViewController.h"
#import "AGTBook.h"
#import "AGTSimplePDFViewController.h"



@interface AGTBookViewController ()

@end

@implementation AGTBookViewController

-(id) initWithModel:(AGTBook*) book{
    if (self = [super initWithNibName:nil
                               bundle:nil]) {
        _model = book;
        
        
    }
    
    return self;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    // Asegurarme de que la vista del controlador ocupa solo el espacio
    // que deje un navigation o un tabBar
    self.edgesForExtendedLayout = UIRectEdgeNone;
    
    
    
}

-(void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    // Alta en Notificaciones del libro para enterarme de cuando
    // cambian cosas. tambien podría usar KVO
    [self observeBookNotifications];

    // Sincronizamos modelo -> vistas
    [self syncWithModel];
    
    // Si estoy dentro de un SplitVC me pongo el botón
    self.navigationItem.leftBarButtonItem = self.splitViewController.displayModeButtonItem;

}

-(void) viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    
    // baja en notificaciones del libro
    [self removeBookObserver];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -  Notificaciones
-(void) observeBookNotifications{
    NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
    [nc addObserver:self
           selector:@selector(syncWithModel)
               name:AGTBOOK_DID_CHANGE_NOTIFICATION
             object:self.model];
}

-(void) removeBookObserver{
    NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
    [nc removeObserver:self];
}

#pragma mark - Actions
- (IBAction)readBook:(id)sender {

    AGTSimplePDFViewController *pdfVC = [[AGTSimplePDFViewController alloc] initWithModel:self.model];
    
    [self.navigationController pushViewController:pdfVC
                                         animated:YES];
}

- (IBAction)flipFavorite:(id)sender {
    self.model.isFavorite = !self.model.isFavorite;
}


#pragma mark - AGTLibraryTableViewControllerDelegate
-(void) libraryTableViewController:(AGTLibraryTableViewController *)vc
                     didSelectBook:(AGTBook *)newBook{
    
    // cambiamos modelo y sincronizamos con el nuevo
    self.model = newBook;
    [self syncWithModel];
}

#pragma mark - UISplitViewControllerDelegate
-(void) splitViewController:(UISplitViewController *)svc willChangeToDisplayMode:(UISplitViewControllerDisplayMode)displayMode{
    
    // Averiguar si la tabla se ve o no
    if (displayMode == UISplitViewControllerDisplayModePrimaryHidden) {
        
        // La tabla está oculta y cuelga del botón
        // Ponemos ese botón en mi barra de navegación
        self.navigationItem.leftBarButtonItem = svc.displayModeButtonItem;
    }else{
        // Se muestra la tabla: oculto el botón de la
        // barra de navegación
        self.navigationItem.leftBarButtonItem = nil;
    }
    
    
}


#pragma mark -  Utils
-(void) syncWithModel{
    
    self.title = self.model.title;
    
    [UIView transitionWithView:self.coverView
                      duration:0.7
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:^{
                        self.coverView.image = self.model.coverImage;
                    } completion:nil];

    
    
    if (self.model.isFavorite) {
        self.favoriteButton.title = @"★";
    }else{
        self.favoriteButton.title = @"☆";
    }
}
@end

//
//  AGTBook.h
//  HackerBooks
//
//  Created by Fernando Rodríguez Romero on 10/04/15.
//  Copyright (c) 2015 Agbo. All rights reserved.
//

#define AGTBOOK_DID_CHANGE_NOTIFICATION @"AGTBOOK_DID_CHANGE_NOTIFICATION"
#define BOOK_KEY @"BOOK"

@import Foundation;
@import UIKit;

@interface AGTBook : NSObject

@property (nonatomic, copy) NSString *title;
@property (nonatomic, strong) NSArray *authors;
@property (nonatomic, strong) NSArray *tags;
@property (nonatomic, strong) NSURL *pdfURL;
@property (nonatomic, strong) NSURL *coverURL;
@property (nonatomic) BOOL isFavorite;
@property (nonatomic, strong, readonly) UIImage* coverImage;


// designated
-(id) initWithTitle:(NSString*) title
            authors:(NSArray*) authors
               tags:(NSArray*) tagNames
             pdfURL: (NSURL*) pdfURL
           coverURL:(NSURL*) imageURL;

-(id) initWithJSONDictionary:(NSDictionary*) dict;


// "Cosmetic" methods
-(NSString *) formattedListOfAuthors;
-(NSString *) formattedListOfTagNames;



@end

//
//  AGTBookTag.h
//  HackerBooks
//
//  Created by Fernando Rodríguez Romero on 12/04/15.
//  Copyright (c) 2015 Agbo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AGTBookTag : NSObject <NSCopying>

@property (nonatomic, readonly) BOOL isFavorite;

+(instancetype) bookTagWithName:(NSString*)tagName;
+(instancetype) favoriteBookTag;

-(id) initWithName:(NSString*) tagName;

-(NSString*) normalizedName;

- (NSComparisonResult)compare:(AGTBookTag *)tag;
@end

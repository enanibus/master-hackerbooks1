//
//  AGTLibraryTableViewController.m
//  HackerBooks
//
//  Created by Fernando Rodríguez Romero on 13/04/15.
//  Copyright (c) 2015 Agbo. All rights reserved.
//

#import "AGTLibraryTableViewController.h"
#import "AGTLibrary.h"
#import "AGTBookTag.h"
#import "AGTBook.h"
#import "AGTBookTableViewCell.h"
#import "AGTBookSectionTableViewCell.h"
#import "AGTBookViewController.h"

@interface AGTLibraryTableViewController ()
@property (nonatomic, strong) AGTLibrary *model;

@end

@implementation AGTLibraryTableViewController

#pragma mark - Object Lifecycle
-(id) initWithModel:(AGTLibrary *)library style:(UITableViewStyle)style{
    
    if (self = [super initWithStyle:style]) {
        _model = library;
        self.title = @"HackerBooks";
    }
    
    return self;
}

-(void) dealloc{
    [self tearDownNotifications];
}

#pragma mark - View Lifecycle
-(void) viewDidLoad{
    [super viewDidLoad];
    
    // Register nib
    [self registerNib];
    
    // Set height for all cells
    self.tableView.rowHeight = [AGTBookTableViewCell height];
    
    // Alta en notificaciones
    [self setupNotifications];
    
    
    // Asegurarme de que la vista del controlador ocupa solo el espacio
    // que deje un navigation o un tabBar
    self.edgesForExtendedLayout = UIRectEdgeNone;

}



#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return [[self.model tags] count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    // Return the number of rows in the section.
    AGTBookTag *tag = [[self.model tags] objectAtIndex:section];
    return [self.model bookCountForTag:tag.normalizedName];
    
}

-(UIView*) tableView:(UITableView *)tableView
viewForHeaderInSection:(NSInteger)section{
 
    AGTBookSectionTableViewCell *cell = [tableView dequeueReusableHeaderFooterViewWithIdentifier:[AGTBookSectionTableViewCell cellId]];
    cell.titleView.text =[[[self.model tags] objectAtIndex:section]normalizedName];

    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return [AGTBookSectionTableViewCell height];
}


- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    // Averiguar el libro
    NSString *tagName = [[self.model.tags objectAtIndex:indexPath.section] normalizedName];
    AGTBook *book = [self.model bookForTag:tagName
                                   atIndex:indexPath.row];
    
    // Crear la celda
    AGTBookTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:[AGTBookTableViewCell cellId] forIndexPath:indexPath];
    
    // Configurar
    cell.coverView.image = book.coverImage;
    cell.titleView.text = book.title;
    cell.authorsView.text = [book formattedListOfAuthors];
    cell.tagsView.text = [book formattedListOfTagNames];
    [cell observeBook:book];
    
    // Devolver
    return cell;
}

-(void) tableView:(UITableView *)tableView
didEndDisplayingCell:(UITableViewCell *)cell
forRowAtIndexPath:(NSIndexPath *)indexPath{
    
    // Por si acaso y para ahorrar memoria
    // hago de limpieza de las celdas aquí
    // nates de que le llegue prepareForReuse
    AGTBookTableViewCell *bCell = (AGTBookTableViewCell*) cell;
    [bCell cleanUp];
    
}

-(void) tableView:(UITableView *)tableView
didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    AGTBook *selectedBook = [self bookForIndexPath:indexPath];
    
    
    // Envio información al delegado si corresponde
    if ([self.delegate respondsToSelector:@selector(libraryTableViewController:didSelectBook:)]) {
        
        [self.delegate libraryTableViewController:self
                                    didSelectBook:selectedBook];
    }
    
    // Mando también la notificación
    [self postNotificationThatSelectedBookDidChange:selectedBook];
                                    
                                                                                                                    
    
}


#pragma mark - Utils
-(AGTBook*) bookForIndexPath:(NSIndexPath*) indexPath{
    // Averiguo el tag a partir de la sección
    NSString *tagName = [[[self.model tags]
                          objectAtIndex:indexPath.section] normalizedName];
    
    // Con eso averiguo el libro
    return [self.model bookForTag:tagName
                          atIndex:indexPath.row];
}
-(void) registerNib{
    
    UINib *nib = [UINib nibWithNibName:@"AGTBookTableViewCell"
                                bundle:[NSBundle mainBundle]];
    [self.tableView registerNib:nib
         forCellReuseIdentifier:[AGTBookTableViewCell cellId]];
    
    nib = [UINib nibWithNibName:@"AGTBookSectionTableViewCell"
                         bundle:[NSBundle mainBundle]];
    
    [self.tableView registerNib:nib forHeaderFooterViewReuseIdentifier:[AGTBookSectionTableViewCell cellId]];
    
}

#pragma mark -  Notifications
-(void) setupNotifications{
    
    NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
    
    [nc addObserver:self
           selector:@selector(notifyThatTagsDidChange:)
               name:AGTLIBRARY_TAGS_DID_CHANGE_NOTIFICATION
             object:nil];
    
}

-(void) tearDownNotifications{
    
    NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
    [nc removeObserver:self];
}

-(void) postNotificationThatSelectedBookDidChange:(AGTBook*)selectedBook{
    
    // Mando también la notificación
    NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
    
    NSNotification *n = [NSNotification
                         notificationWithName:AGTBOOK_DID_CHANGE_NOTIFICATION
                         object:self
                         userInfo:@{BOOK_KEY:selectedBook}];
    [nc postNotification:n];

}
//AGTBOOK_DID_CHANGE_NOTIFICATION
-(void)notifyThatTagsDidChange:(NSNotification*) notification{
    
    [self.tableView reloadData];
}



#pragma mark - AGTLibraryTableViewControllerDelegate
-(void) libraryTableViewController:(AGTLibraryTableViewController *)vc
                     didSelectBook:(AGTBook *)newBook{
    
    AGTBookViewController *bVC = [[AGTBookViewController alloc]
                                  initWithModel:newBook];
    
    [self.navigationController pushViewController:bVC
                                         animated:YES];
}



@end







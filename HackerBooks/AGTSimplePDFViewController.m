//
//  AGTSimplePDFViewController.m
//  HackerBooks
//
//  Created by Fernando Rodríguez Romero on 17/04/15.
//  Copyright (c) 2015 Agbo. All rights reserved.
//

#import "AGTSimplePDFViewController.h"
#import "AGTBook.h"
#import "AGTLibraryTableViewController.h"

@interface AGTSimplePDFViewController ()

@end

@implementation AGTSimplePDFViewController

-(id)initWithModel:(AGTBook*) book{
    
    if (self = [super initWithNibName:nil
                               bundle:nil]) {
        _model = book;
        self.title = book.title;
    }
    
    return self;
}

-(void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    self.activityView.hidden = YES;
    
    [self syncWithModel];
    
    // Alta en notificaciones de library
    NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
    [nc addObserver:self
           selector:@selector(notifyThatBookDidChange:)
               name:AGTBOOK_DID_CHANGE_NOTIFICATION
             object:nil];
}

-(void) viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    // Baja en notificaciones
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    
}

#pragma mark - Notificaciones
-(void) notifyThatBookDidChange:(NSNotification *) notification{
    
    // sacamos el nuevo libro
    AGTBook *newBook = [notification.userInfo objectForKey:BOOK_KEY];
    self.model = newBook;
    [self syncWithModel];
}


#pragma mark - Util
-(void) syncWithModel{
    
    self.title = self.model.title;
    
    NSFileManager *fm = [NSFileManager defaultManager];
    
    NSURL *localURL = [self localURLForRemoteURL:self.model.pdfURL];
    if ([fm fileExistsAtPath:[localURL path]]) {
        [self.pdfView loadData:[NSData dataWithContentsOfURL:localURL]
                      MIMEType:@"application/pdf"
              textEncodingName:@"UTF-8"
                       baseURL:nil];
        
    }else{
        // No está en local
        // Hay que descargar y guardar
        self.activityView.hidden = NO;
        [self.activityView startAnimating];
        
        dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0), ^{
            NSData *data = [NSData dataWithContentsOfURL:self.model.pdfURL];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.pdfView loadData:data
                              MIMEType:@"application/pdf"
                      textEncodingName:@"UTF-8"
                               baseURL:nil];
                
                [self.activityView stopAnimating];
                self.activityView.hidden = YES;
                
                [data writeToURL:localURL
                      atomically:YES];
            });
        });
    }
    
}


-(NSURL*)documentsDirectory{
    NSFileManager *fm = [NSFileManager defaultManager];
    
    NSURL *docs = [[fm URLsForDirectory:NSDocumentDirectory
                              inDomains:NSUserDomainMask] lastObject];
    return docs;
}

-(NSURL *) localURLForRemoteURL:(NSURL*) remoteURL{
    
    NSString *fileName = [remoteURL lastPathComponent];
    NSURL *local = [[self documentsDirectory] URLByAppendingPathComponent:fileName];
    
    return local;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end

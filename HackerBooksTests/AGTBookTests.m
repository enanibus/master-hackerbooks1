//
//  AGTBookTests.m
//  HackerBooks
//
//  Created by Fernando Rodríguez Romero on 11/04/15.
//  Copyright (c) 2015 Agbo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <XCTest/XCTest.h>
#import "AGTBook.h"
#import "AGTLibrary.h"
#import "AGTBookTag.h"

@interface AGTBookTests : XCTestCase
@property (nonatomic, strong) NSBundle *testBundle;
@property (nonatomic, strong) NSData *jsonBook;
@property (nonatomic, strong) NSData *jsonLibrary;
@property (nonatomic, strong) AGTLibrary *library;



@end

@implementation AGTBookTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
    self.testBundle = [NSBundle bundleForClass:[self class]];
    self.jsonBook = [NSData dataWithContentsOfURL:[self.testBundle URLForResource:@"book" withExtension:@"json"]];
    self.jsonLibrary = [NSData dataWithContentsOfURL:[self.testBundle URLForResource:@"library" withExtension:@"json"]];
    
    self.library = [[AGTLibrary alloc] initWithJSONData:self.jsonLibrary];
    
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
    self.testBundle = nil;
    self.jsonLibrary = nil;
    self.jsonBook = nil;
    
}

-(void) testThatMakingAFirstFavoriteIncreasesNumberOfTagsByOne{
    
    NSUInteger oldTagCount = [self.library.tags count];
    
    AGTBook *fringeBook = [self.library bookForTag:@"fringe"
                                           atIndex:0];
    AGTBook *designBook = [self.library bookForTag:@"design"
                                           atIndex:0];
    NSLog(@"fringeBooks %lu", (unsigned long)[self.library bookCountForTag:@"fringe"]);
    NSLog(@"designBooks %lu", (unsigned long)[self.library bookCountForTag:@"design"]);

    
    fringeBook.isFavorite = YES;
    XCTAssertEqual([self.library.tags count],
                   oldTagCount + 1, @"Should have increased by one!");
    
    designBook.isFavorite = YES;
    XCTAssertEqual([self.library.tags count],
                   oldTagCount + 1, @"SHould have remiained equal");
    
    fringeBook.isFavorite = NO;
    
    NSLog(@"fringeBooks %lu", (unsigned long)[self.library bookCountForTag:@"fringe"]);
    NSUInteger fringeCount = [self.library bookCountForTag:@"fringe"];
    XCTAssertEqual(fringeCount, 1);
    
    designBook.isFavorite = NO;
    NSLog(@"designBooks %lu", (unsigned long)[self.library bookCountForTag:@"design"]);
    XCTAssertEqual([self.library bookCountForTag:@"design"], 1);
    XCTAssertEqual([self.library.tags count],
                   oldTagCount, @"SHould have been reduced by one");
    
    
}
-(void) testInitFromDict{
    
    NSError *err;
    NSDictionary *bookDict = [NSJSONSerialization JSONObjectWithData:self.jsonBook
                                                             options:0
                                                               error:&err];
    if (!bookDict) {
        XCTFail(@"Error reading json data\n%@", err);
    }
    
    XCTAssertTrue([bookDict isKindOfClass:[NSDictionary class]]);
    
    AGTBook *book = [[AGTBook alloc] initWithJSONDictionary:bookDict];
    
    XCTAssertNotNil(book.title);
    XCTAssertNotNil(book.authors);
    XCTAssertNotNil(book.tags);
    XCTAssertNotNil(book.coverURL);
    XCTAssertNotNil(book.pdfURL);
    
    XCTAssertEqual(3, book.authors.count);
    XCTAssertEqualObjects(@"Trinna Chiasson",
                          [book.authors firstObject],
                          @"White spaces not being removed");
    XCTAssertEqual(3, book.tags.count);
    
    XCTAssertEqualObjects([NSURL class], [book.coverURL class], @"URLS should be urls...");
    XCTAssertEqualObjects([NSURL class], [book.pdfURL class], @"URLS should be urls...");

}

-(void) testSizeOfLibrary{
    
    XCTAssertEqual(30, self.library.bookCount);
    
}

-(void) testThatTagsAreSorted{
    
    NSError *err;
    NSDictionary *bookDict = [NSJSONSerialization JSONObjectWithData:self.jsonBook
                                                             options:0
                                                               error:&err];
    if (!bookDict) {
        XCTFail(@"Error reading json data\n%@", err);
    }

    NSArray *sorted = @[[AGTBookTag bookTagWithName:@"favorite"],
                        [AGTBookTag bookTagWithName:@"data visualization"],
                        [AGTBookTag bookTagWithName:@"design"]];
    
    AGTBook *book = [[AGTBook alloc] initWithJSONDictionary:bookDict];
    
    XCTAssertEqualObjects(sorted, [book tags]);
    XCTAssertTrue([[[book tags] firstObject]isFavorite]);
}

-(void)testThatTagsInLibraryAreSorted{
    
    XCTAssertEqualObjects(self.library.tags,
                          [self.library.tags sortedArrayUsingSelector:@selector(compare:)]);
    
}

-(void) testThatAFavoriteTagIsUnique{
   
    NSError *err;
    NSDictionary *bookDict = [NSJSONSerialization JSONObjectWithData:self.jsonBook
                                                             options:0
                                                               error:&err];
    if (!bookDict) {
        XCTFail(@"Error reading json data\n%@", err);
    }

    AGTBook *book = [[AGTBook alloc] initWithJSONDictionary:bookDict];
    book.isFavorite = YES;
    book.isFavorite = YES;
    book.isFavorite = YES;
    
    XCTAssertTrue(book.isFavorite);
    
    __block int count = 0;
    [book.tags enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        if ([obj isFavorite]) {
            count++;
        }
    }];
    
    XCTAssertEqual(1, count, @"MacLeod, there can only be one!");
    
    book.isFavorite = NO;
    
    count = 0;
    [book.tags enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        if ([obj isFavorite]) {
            count++;
        }
    }];
    
    XCTAssertEqual(0, count, @"There should be none");
    
}

-(void) testSizeOfTags{
    XCTAssertEqual(63, self.library.tags.count);
}

-(void) testNumberOfBooksForTag{
    
    XCTAssertEqual(1, [self.library bookCountForTag:@"fringe"]);
    
}

-(void) testBooksForTagRetrieval{
    
    NSArray *fringeBooks = [self.library booksForTag:@"fringe"];
    XCTAssertNotNil(fringeBooks, @"There should be an array");
    XCTAssertEqual([fringeBooks count], 1, @"There should only be one book on fringe");
    XCTAssertEqualObjects([[[fringeBooks firstObject] authors]firstObject],
                          @"Christa Faust");
    
    XCTAssertNil([self.library booksForTag:@"paleontology"], @"The should be no books on paleontology");
    
    
    XCTAssertNil([self.library bookForTag:@"paleontology"
                                  atIndex:2], @"There should be nothing there");
}

-(void) testThatTagsInArrayAreAGTTags{
    
    id tag = [[self.library tags] lastObject];
    XCTAssertEqualObjects([AGTBookTag class], [tag class]);
}




@end

//
//  AGTLibraryTableVCTests.m
//  HackerBooks
//
//  Created by Fernando Rodríguez Romero on 13/04/15.
//  Copyright (c) 2015 Agbo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <XCTest/XCTest.h>

#import "AGTLibrary.h"
#import "AGTBook.h"
#import "AGTLibraryTableViewController.h"
#import "AGTBookTag.h"

@interface AGTLibraryTableVCTests : XCTestCase
@property (nonatomic, strong) NSBundle *testBundle;
@property (nonatomic, strong) NSData *jsonLibrary;
@property (nonatomic, strong) AGTLibrary *library;
@property (nonatomic, strong) AGTLibraryTableViewController *sut;

@end

@implementation AGTLibraryTableVCTests

- (void)setUp {
    [super setUp];
    
    self.testBundle = [NSBundle bundleForClass:[self class]];
    self.jsonLibrary = [NSData dataWithContentsOfURL:[self.testBundle URLForResource:@"library" withExtension:@"json"]];
    
    self.library = [[AGTLibrary alloc] initWithJSONData:self.jsonLibrary];
    self.sut = [[AGTLibraryTableViewController alloc] initWithModel:self.library
                                                              style:UITableViewStylePlain];
    
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
    
    self.testBundle = nil;
    self.jsonLibrary = nil;
    self.sut = nil;
    
}

-(void) testNumberOfSections{
    
    XCTAssertEqual([self.sut numberOfSectionsInTableView:nil],
                   [[self.library tags] count]);
    
}

-(void) testNumberOfRowsInSection{
    
    NSArray *books = [self.library booksForTag:@"fringe"];
    [books enumerateObjectsUsingBlock:^(AGTBook * each, NSUInteger idx, BOOL *stop) {
        [each setIsFavorite:YES ];
    }];
    
    XCTAssertEqual([self.sut tableView:nil
                 numberOfRowsInSection:0], [books count]);
    
}





@end

//
//  AGTBookSectionTableViewCell.h
//  HackerBooks
//
//  Created by Fernando Rodríguez Romero on 14/04/15.
//  Copyright (c) 2015 Agbo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AGTBookSectionTableViewCell : UITableViewHeaderFooterView
@property (weak, nonatomic) IBOutlet UILabel *titleView;

+(CGFloat) height;
+(NSString*) cellId;

@end
